import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerTesting {

    private static final Logger LOGGER = Logger.getLogger(LoggerTesting.class.getName());

    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                throw new IllegalArgumentException("Et et et, 2 aja bro.");
            }

            double num1 = Double.parseDouble(args[0]);
            double num2 = Double.parseDouble(args[1]);

            if (num2 == 0) {
                throw new ArithmeticException("Tak boleh yaaa bagi sama 0. Okay?");
            }

            double result = num1 / num2;
            System.out.println("Hasil: " + result);
        } catch (ArithmeticException | IllegalArgumentException e) {
            System.out.println(e);
            LOGGER.log(Level.SEVERE, "Hello {0} {1}", new Object[]{Level.SEVERE.getName(), LoggerTesting.class.getName()});
        }
        LOGGER.log(Level.INFO, "Hello {0} logger{1}", new Object[]{Level.INFO.getName(), LoggerTesting.class.getName()});

    }
}