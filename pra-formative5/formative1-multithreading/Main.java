/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        for(int i = 0; i <10; i++){
            CustomThread thread = new CustomThread();
            thread.start();

            if(thread.getState() == Thread.State.RUNNABLE) {
                System.out.println("Thread #" + thread.threadId() + " is running.");
            }
        }
    }
}